<div align="center">

<img src="./assets/dockrunners-banner-1024px.jpg" />
<br/>

# DockRunners

**Manage GitLab Runners in Docker via REST**

</div>

If you're running GitLab runners in Docker and you're looking for a simple, non-manual way of doing so, DockRunners has you covered. You can run DockRunners in your Docker alongside the GitLab Runners control service - it just needs access to the configuration file containing Runner configs and things will _just work™_.


## Quickstart

_some instructions for how to run it locally for testing_


## Motivation

Running GitLab Runners in Docker is simple, but managing them - adding new ones, cleaning up, rotating them - is an annoying manual task. If you're anything like us, you don't like those. Moreover, it's something we don't do _that often_ - a few times per month perhaps - so we end up looking up the manual steps in the GitLab docs every godforsaken time. Meanwhile, pretty much everything else we run is Terraformed, which makes this doubl-y annoying.

Thus, we wrote this simple service which provides a CRUD API for GitLab Runner configurations and can run in Docker, managing the Runners configuration file. We also have [a Terraform provider]() you can use to _GitLab Runner Setup as Code_ your Runners.


## Using DockRunners

_instructions on how to get set up for real, and a link to an actual example_


## Local Development Setup

It's a fairly vanilla [PDM](https://pdm-project.org/latest/) project, so if you have that up and running, you should be good. We further recommend using [pyenv](https://github.com/pyenv/pyenv) for you base Python setup, and [pipx](https://github.com/pypa/pipx) to install things in your pyenv managed venv. But hey, I mean, you do you.

### Prerequisites

- well, git
- some Python 3.10+
- PDM

### Setup

1. Clone the thing.
   ```bash
   git clone git@github.com:bmc-labs/DockRunners.git
   cd DockRunners
   ```
1. Install things and activate the venv:
   ```bash
   pdm install && source venv-activate
   ```

That's it. Make a PR with your changes and we'll talk about them.


## Support

This is an open source project, so there isn't support per se. If you open an issue in the repository, we'll try and help you, but no promises.

---

<div align="center">
© Copyright 2024 <b>bmc::labs</b> GmbH. All rights reserved.<br />
<em>solid engineering. sustainable code.</em>
</div>
