from fastapi import FastAPI
from pydantic import BaseModel
from fastapi.encoders import jsonable_encoder
import logging
import sqlite3
import subprocess

log = logging.getLogger(__name__)
app = FastAPI()


# result = subprocess.run(['command'], capture_output=True, text=True)
# capture_output=True enables the capturing of the commands output.
# text= True command ensures the output is returned as a string.


class Runner(BaseModel):
    id: int
    url: str
    token: str
    image: str
    description: str
    tag_list: str
    run_untagged: bool


migration = """
CREATE TABLE IF NOT EXISTS runners (
    id           INTEGER PRIMARY KEY,
    url          TEXT                NOT NULL,
    token        TEXT                UNIQUE NOT NULL,
    description  TEXT                NOT NULL,
    image        TEXT                NOT NULL,
    tag_list     TEXT                NOT NULL,
    run_untagged BOOLEAN             NOT NULL
);
"""

con = sqlite3.connect(":memory:", check_same_thread=False)
cur = con.cursor()
cur.execute(migration)


@app.post("/", status_code=201)
def create(runner: Runner):
    log.info("saving new runner")
    log.debug(f"runner={runner}")

    cur.execute(
        f"""
        INSERT INTO runners
        VALUES (
            {runner.id}, 
            '{runner.url}',
            '{runner.token}', 
            '{runner.description}', 
            '{runner.image}',
            '{runner.tag_list}', 
            {runner.run_untagged}
        );
        """
    )
    con.commit()
    return


@app.get("/{id}")
def read_runner(id: int, q: str | None = None):
    result = subprocess.run(["gitlab-runner list"], capture_output=True, text=True)
    log.info(result.stdout)
    log.info("read id from selected runner")
    res = cur.execute(f"SELECT * FROM runners WHERE id={id};")
    runners = res.fetchone()
    return runners


@app.get("/")
async def read_all():
    result = subprocess.run(["gitlab-runner verify"], capture_output=True, text=True)
    log.info(result.stdout)
    log.info("reading all runners")

    res = cur.execute("SELECT * FROM runners;")
    runners = res.fetchall()

    log.debug(f"runners={runners}")
    return runners


@app.put("/{id}")
def update(id: int, runner: Runner):
    result = subprocess.run(["gitlab-runner list"], capture_output=True, text=True)
    log.info(result.stdout)
    res = cur.execute(
        f"""
        UPDATE runners
        SET id = {runner.id},
            url = '{runner.url}',
            token = '{runner.token}',
            description = '{runner.description}',
            image = '{runner.image}',
            tag_list = '{runner.tag_list}',
            run_untagged = '{runner.run_untagged}'
        WHERE id={id};
        """
    )
    runners = res.fetchone()
    result = subprocess.run(["gitlab-runner status"], capture_output=True, text=True)
    log.info(result.stdout)
    return runners


# @app.delete("/")
# async def delete_all(id: int):
#     log.info("delete all runner")

#     res = cur.execute("""DELETE FROM runners;""")
#     runners = res.fetchone()

#     return runners


@app.delete("/{id}")
async def delete_one(id: int):
    log.info("delete one runner")

    res = cur.execute(f"DELETE FROM runners WHERE id={id};")
    rows_modified = res.fetchone()

    return f"modified {rows_modified} rows"
