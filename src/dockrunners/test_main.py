from fastapi.testclient import TestClient

from .main import app

client = TestClient(app)


def test_read_main():
    response = client.get("/")
    assert response.status_code == 200


def test_create():
    response = client.get(
        "/",
        json={
            "id": 1,
            "url": "warbl.garbl",
            "token": "warbl",
            "description": "warblgarbl",
            "image": "warbgarb",
            "tag_list": "wgarb",
            "run_untagged": False,
        },
    )
    assert response.status_code == 201
    assert response.json() == {
        "id": 1,
        "url": "warbl.garbl",
        "token": "warbl",
        "description": "warblgarbl",
        "image": "warbgarb",
        "tag_list": "wgarb",
        "run_untagged": False,
    }

    response = client.post(
        "/",
        json={
            "id": 1,
            "url": "warbl.garbl",
            "token": "warbl",
            "description": "warblgarbl",
            "image": "warbgarb",
            "tag_list": "wgarb",
            "run_untagged": False,
        },
    )
    assert response.status_code == 201

    response = client.put(
        "/",
        json={
            "id": 1,
            "url": "warbl.garbl",
            "token": "warbl",
            "description": "warblgarbl",
            "image": "warbgarb",
            "tag_list": "wgarb",
            "run_untagged": False,
        },
    )
    assert response.status_code == 200

    response = client.delete(
        "/",
        json={
            "id": 1,
            "url": "warbl.garbl",
            "token": "warbl",
            "description": "warblgarbl",
            "image": "warbgarb",
            "tag_list": "wgarb",
            "run_untagged": False,
        },
    )
    assert response == "modified None rows"
